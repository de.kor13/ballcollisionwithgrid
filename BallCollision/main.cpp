#include "SFML/Graphics.hpp"
#include "MiddleAverageFilter.h"
#include "ball.h"
#include <cmath>
#include <iostream>
#include <memory>


int WINDOW_X = 1024;
int WINDOW_Y = 768;
constexpr int MAX_BALLS = 500;
constexpr int MIN_BALLS = 300;

Math::MiddleAverageFilter<float,100> fpscounter;

class Cell;
class Grid;




void handleCollisions(Grid& grid)
{
    // проверка клеток
    for (int i = 0; i < grid.m_sell.size(); i++) {
        Cell& cell = grid.m_sell[i];

        // итерация по всем шарам
        for (int j = 0; j < cell.balls.size(); j++) {
            Ball& ball1 = *cell.balls[j];

            // Проверка соседних клеток
            for (int dx = -1; dx <= 1; dx++) {
                for (int dy = -1; dy <= 1; dy++) {
                    if (dx == 0 && dy == 0) continue;

                    int neighborX = (ball1.ownerCell - &grid.m_sell[0]) % grid.m_numXCells + dx;
                    int neighborY = (ball1.ownerCell - &grid.m_sell[0]) / grid.m_numXCells + dy;
                    if (neighborX < 0 || neighborX >= grid.m_numXCells || neighborY < 0 || neighborY >= grid.m_numYCells) {
                        continue;
                    }

                    Cell& neighborCell = grid.m_sell[neighborY * grid.m_numXCells + neighborX];
                    for (int k = 0; k < neighborCell.balls.size(); k++) {
                        Ball& ball2 = *neighborCell.balls[k];
                        if (&ball1 == &ball2) continue;

                        // проверка коллизии
                        ball1.collide(ball2);
                    }
                }
            }
        }
    }
}


 void draw_fps(sf::RenderWindow& window, float fps)
 {
     char c[32];
    snprintf(c, 32, "FPS: %f", fps);
     std::string string(c);
     sf::String str(c);
     window.setTitle(str);
 }







int main()
{
    sf::RenderWindow window(sf::VideoMode(WINDOW_X, WINDOW_Y), "ball collision demo");
    srand(time(NULL));

    std::vector<Ball> balls;
    

    for (size_t i = 0; i < (rand() % (MAX_BALLS - MIN_BALLS) + MIN_BALLS); i++){
        Ball newBall(i);
        newBall.setPosition(rand() % WINDOW_X,rand() % WINDOW_Y);
        newBall.setDirection((-5 + (rand() % 10)) / 3.,(-5 + (rand() % 10)) / 3.);
        newBall.setRadius(5 + rand() % 5);
        newBall.setMass(newBall._r);  
        //newBall.setMass(1);
        newBall.setSpeed(30 + rand() % 30);
        balls.push_back(newBall);

    }

    auto max_r = std::max_element(balls.begin(), balls.end(),
                             []( Ball &a, Ball &b )
                             {
                                return a.getRadius() < b.getRadius();
                             } ); 

    // Grid grid(WINDOW_X, WINDOW_Y, max_r->getRadius()); 
    // std::unique_ptr<Grid> grid = std::make_unique<Grid>(WINDOW_X, WINDOW_Y, max_r->getRadius());
    Grid grid(WINDOW_X, WINDOW_Y, max_r->getRadius());
     for (auto& ball : balls){
        grid.addBall(&ball);
     }




    window.setFramerateLimit(60);

    sf::Clock clock;
    float lastime = clock.restart().asSeconds();

    while (window.isOpen())
    {

        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
        }

        float current_time = clock.getElapsedTime().asSeconds();
        float deltaTime = current_time - lastime;
        fpscounter.push(1.0f / (current_time - lastime));
        lastime = current_time;

        

        for (auto &ball : balls)
        {
          ball.move(deltaTime, grid);
         }

        handleCollisions(grid);

       window.clear();
         for (auto ball : balls)
         {
            ball.draw(window);
         }

        

		draw_fps(window, fpscounter.getAverage());
        
		window.display();
    }
    return 0;
}

