class Cell;
class Grid;
#include "ball.h"

extern int WINDOW_X ;
extern int WINDOW_Y ;

void Ball::collide(Ball& other)
{
    // Вычислите расстояние между центрами двух шаров.
    sf::Vector2f delta = other.getPosition() - getPosition();

    float distance = std::sqrt(delta.x * delta.x + delta.y * delta.y);

    // проверка столкновения
    if (distance < getRadius() + other.getRadius()) {
        // Вычислите угол между центрами двух шаров
        float angle = std::atan2(delta.y, delta.x);

        // Вычислите общую массу двух шаров
        float mass = getMass() + other.getMass();

        // скорость каждого шара вдоль оси столкновения
        float v1 = getSpeed() * std::cos(angle);
        float v2 = other.getSpeed() * std::cos(angle);

        // скорости каждого шара перпендикулярные оси столкновения
        float vp1 = getSpeed() * std::sin(angle);
        float vp2 = other.getSpeed() * std::sin(angle);

        // новые скорости вдоль оси столкновения
        float new_v1 = ((getMass() - other.getMass()) * v1 + 2 * other.getMass() * v2) / mass;
        float new_v2 = ((other.getMass() - getMass()) * v2 + 2 * getMass() * v1) / mass;


        // обнавление скорости и направления 
        setDirection(std::cos(angle), std::sin(angle));
        setSpeed(new_v1);

        other.setDirection(std::cos(angle + M_PI), std::sin(angle + M_PI));
        other.setSpeed(new_v2);

        // раздвгание шаров 
        
         float overlap = getRadius() + other.getRadius() - distance;
         float dx = overlap / 4 * std::cos(angle);
         float dy = overlap / 4 * std::sin(angle);
         updatePosition(-dx, -dy);
         other.updatePosition(dx, dy);
    }
}




void Ball::edges(){
  if(this->_p.x - this->_r < 0 || this->_p.x + this->_r > WINDOW_X ){
       // _speed*=-1;
        _dir.x*=-1;
    }
    if(this->_p.y - this->_r < 0 ||this->_p.y + this->_r > WINDOW_Y){
        //_speed*=-1;
        _dir.y*=-1;
    }

}

void Ball::updatePosition(float dx,float dy){
    _p.x += dx;
    _p.y += dy;
}


void Ball::draw(sf::RenderWindow& window)
{
    sf::CircleShape gball;
    gball.setRadius(this->getRadius());
    gball.setPosition(this->getPosition().x, this->getPosition().y);
    window.draw(gball);
}

void Ball::move(float deltaTime, Grid& grid)
{ 
    edges();

   
    sf::Vector2f displacement = _dir * _speed * deltaTime;
    updatePosition(displacement.x, displacement.y);

  
    Cell* newCell = grid.getCell(_p);
    if (newCell != ownerCell) {
        if (ownerCell) {
            grid.removeBallFromCell(this);
        }
        grid.addBall(this, newCell);
    }


    

}