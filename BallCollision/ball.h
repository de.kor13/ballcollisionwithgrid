#include "SFML/Graphics.hpp"
#include <cmath>



extern int WINDOW_X ;
extern int WINDOW_Y ;

class Cell;
class Grid;



class Ball{
   public:
        sf::Vector2f _p;
        sf::Vector2f _dir;
        
        float _r = 0;
        float _mass = 0;
        float _speed = 0;
        size_t _id={};
        Cell* ownerCell = nullptr;
        int cellIndex = -1;

 
        Ball(size_t id):_id(id){
         //   std::cout<<"Ball()"<<std::endl;
        }

        ~Ball(){
       //        std::cout<<"~Ball()"<<std::endl;
        }


        void draw(sf::RenderWindow& window);
        void move(float deltaTime, Grid& grid);
        void updatePosition(float dx,float dy);
        void edges();
        void collide(Ball& other);


        sf::Vector2f& getPosition(){return _p;}
        sf::Vector2f& getDirection(){return _dir;}
        float getRadius(){return _r;}
        float getSpeed(){return _speed;}
        float getMass(){return _mass;}

        void setPosition(float x,float y){
            _p.x=x;
            _p.y=y;
        }

        void setDirection(float x,float y){
            _dir.x=x;
            _dir.y=y;
            }
        void setRadius(float radius){_r=radius;}
        void setSpeed(float speed){_speed=speed;}
        void setMass(float mass){_mass=mass;}

        size_t getId(){
            return _id;
        }

        

};



struct Cell{
    std::vector<Ball*> balls;
};

class Grid
{
public:
  Grid(int width, int hight, int cellSize) : 
  m_sellSize(cellSize),
  m_hight(hight),
  m_width(width)
  {
    m_numXCells = ceil(WINDOW_X/cellSize);
    m_numYCells = ceil(WINDOW_Y/cellSize);
    m_sell.resize(m_numXCells * m_numYCells);
    
  }
  ~Grid(){
  
        
  }
  Cell* getCell(int x, int y){
    if (x < 0) x = 0;
    if (x >= m_numXCells) x = m_numXCells - 1;
    if (y < 0) y = 0;
    if (y >= m_numYCells) y = m_numYCells - 1;


   // std::cout << y * m_numXCells + m_numXCells <<std::endl;

    return &m_sell[y * m_numXCells + x];

  }
    Cell* getCell(sf::Vector2f& pos){

    int cellX = (int)(pos.x / m_sellSize);
    int cellY = (int)(pos.y / m_sellSize);

    return getCell(cellX,cellY);

  }

  void addBall( Ball* ball){
    Cell* cell = getCell(ball->getPosition());
    cell->balls.push_back(ball);
    ball->ownerCell = cell;
    ball->cellIndex = cell->balls.size()-1;

  }

  void addBall(Ball* ball, Cell* cell) {
    cell->balls.push_back(ball);
    ball->ownerCell = cell;
    ball->cellIndex = cell->balls.size() - 1;
    
}

  void removeBallFromCell(Ball* ball){
    std::vector<Ball*>& balls = ball->ownerCell->balls;
    balls[ball->cellIndex] = balls.back();
    balls.pop_back();
     if ((size_t)ball->cellIndex < balls.size()) {
        balls[ball->cellIndex]->cellIndex = ball->cellIndex;
    }
    ball->cellIndex = -1;
    ball->ownerCell = nullptr;
  }


std::vector<Cell> m_sell; 
  int m_numXCells;
  int m_numYCells;
  int m_sellSize;
private:
  
  
  int m_width;
  int m_hight;

};